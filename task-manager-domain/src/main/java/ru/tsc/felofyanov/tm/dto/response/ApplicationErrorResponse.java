package ru.tsc.felofyanov.tm.dto.response;

import org.jetbrains.annotations.NotNull;

public final class ApplicationErrorResponse extends AbstractResultResponse {

    public ApplicationErrorResponse() {
        setSuccess(false);
    }

    public ApplicationErrorResponse(@NotNull Throwable throwable) {
        super(throwable);
    }
}
