package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataSaveJsonFasterXmlRequest extends AbstractUserRequest {

    public DataSaveJsonFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
