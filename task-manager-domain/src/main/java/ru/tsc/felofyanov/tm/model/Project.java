package ru.tsc.felofyanov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.model.IWBS;
import ru.tsc.felofyanov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractUserOwnerModel implements IWBS {

    private static final long serialVersionUID = 1;

    public Project(@Nullable String userId, @NotNull String name) {
        super(userId, name);
    }

    public Project(@Nullable String userId, @NotNull String name, @NotNull String description) {
        super(userId, name, description);
    }

    public Project(@NotNull String name, @NotNull Status status, @NotNull Date created) {
        super(name, status, created);
    }
}
