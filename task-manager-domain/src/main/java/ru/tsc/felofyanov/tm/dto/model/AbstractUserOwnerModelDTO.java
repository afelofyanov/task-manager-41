package ru.tsc.felofyanov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractUserOwnerModelDTO extends AbstractModelDTO {

    @Nullable
    @Column(name = "user_id")
    private String userId;

    public AbstractUserOwnerModelDTO(@Nullable String userId) {
        this.userId = userId;
    }
}
