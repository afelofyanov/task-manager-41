package ru.tsc.felofyanov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.model.IWBS;
import ru.tsc.felofyanov.tm.enumerated.Status;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public class ProjectDTO extends AbstractWbsDTO implements IWBS {

    private static final long serialVersionUID = 1;

    public ProjectDTO(@Nullable final String userId, @NotNull final String name) {
        super(userId, name);
    }

    public ProjectDTO(@Nullable final String userId, @NotNull final String name, @NotNull final String description) {
        super(userId, name, description);
    }

    public ProjectDTO(@NotNull final String name, @NotNull final Status status, @NotNull final Date created) {
        super(name, status, created);
    }

}
