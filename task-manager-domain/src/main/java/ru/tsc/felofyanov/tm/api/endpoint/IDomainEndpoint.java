package ru.tsc.felofyanov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IDomainEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, NAMESPACE, PART, IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, NAMESPACE, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataLoadBackupResponse loadDataBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull DataLoadBackupRequest request
    );

    @NotNull
    @WebMethod
    DataLoadBase64Response loadDataBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull DataLoadBase64Request request
    );

    @NotNull
    @WebMethod
    DataLoadBinaryResponse loadDataBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull DataLoadBinaryRequest request
    );

    @NotNull
    @WebMethod
    DataLoadJsonFasterXmlResponse loadDataJsonFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataLoadJsonFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataLoadJsonJaxbResponse loadDataJsonJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataLoadJsonJaxbRequest request
    );

    @NotNull
    @WebMethod
    DataLoadXmlFasterXmlResponse loadDataXmlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataLoadXmlFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataLoadXmlJaxbResponse loadDataXmlJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataLoadXmlJaxbRequest request
    );

    @NotNull
    @WebMethod
    DataLoadYamlFasterXmlResponse loadDataYamlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataLoadYamlFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataSaveBackupResponse saveDataBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull DataSaveBackupRequest request
    );

    @NotNull
    @WebMethod
    DataSaveBase64Response saveDataBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull DataSaveBase64Request request
    );

    @NotNull
    @WebMethod
    DataSaveBinaryResponse saveDataBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull DataSaveBinaryRequest request
    );

    @NotNull
    @WebMethod
    DataSaveJsonFasterXmlResponse saveDataJsonFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataSaveJsonFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataSaveJsonJaxbResponse saveDataJsonJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataSaveJsonJaxbRequest request
    );

    @NotNull
    @WebMethod
    DataSaveXmlFasterXmlResponse saveDataXmlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataSaveXmlFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataSaveXmlJaxbResponse saveDataXmlJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataSaveXmlJaxbRequest request
    );

    @NotNull
    @WebMethod
    DataSaveYamlFasterXmlResponse saveDataYamlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataSaveYamlFasterXmlRequest request
    );
}
