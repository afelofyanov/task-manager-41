package ru.tsc.felofyanov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class AbstractUserOwnerModel extends AbstractModel {

    @Nullable
    private String userId;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    public AbstractUserOwnerModel(@Nullable String userId, @NotNull String name) {
        this.userId = userId;
        this.name = name;
    }

    public AbstractUserOwnerModel(@Nullable String userId, @NotNull String name, @NotNull String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    public AbstractUserOwnerModel(@NotNull String name, @NotNull Status status, @NotNull Date created) {
        this.name = name;
        this.status = status;
        this.created = created;
    }

    @Override
    public String toString() {
        return name + " (" + getId() + ") : " + getStatus().getDisplayName();
    }
}
