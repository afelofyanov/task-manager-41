package ru.tsc.felofyanov.tm.exception.system;

import ru.tsc.felofyanov.tm.exception.AbstractExcception;

public class LockException extends AbstractExcception {
    public LockException() {
        super("ERROR! User is lock...");
    }
}
