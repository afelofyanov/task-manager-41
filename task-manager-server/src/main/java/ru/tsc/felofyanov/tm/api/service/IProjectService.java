package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.model.Project;

public interface IProjectService extends IUserOwnerService<Project> {

}
