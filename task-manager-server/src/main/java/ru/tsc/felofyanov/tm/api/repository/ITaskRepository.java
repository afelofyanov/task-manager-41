package ru.tsc.felofyanov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IUserOwnerRepository<Task> {

    @Insert("INSERT INTO tm_task (id, name, description, user_id, project_id, status, created, start_dt, end_dt) " +
            " VALUES (#{id}, #{name}, #{description}, #{userId}, #{projectId}, #{status}, #{created}, #{dateBegin}, " +
            "#{dateEnd})")
    void add(@Nullable Task model);

    @Insert("INSERT INTO tm_task (id, name, description, user_id, project_id, status, created, start_dt, end_dt) " +
            " VALUES (#{id}, #{name}, #{description}, #{userId}, #{projectId}, #{status}, #{created}, #{dateBegin}, " +
            "#{dateEnd})")
    void addAll(@Nullable final Collection<Task> collection);

    @Insert("INSERT INTO tm_task (id, name, description, user_id, project_id, status, created, start_dt, end_dt) " +
            " VALUES (#{id}, #{name}, #{description}, #{userId}, #{projectId}, #{status}, #{created}, #{dateBegin}, " +
            "#{dateEnd})")
    void addByUserId(@Param("userId") @Nullable String userId, @Nullable Task model);

    @Update("UPDATE tm_task SET name = #{name}, description = #{description}, user_id = #{userId}, " +
            "project_id = #{projectId}, status = #{status}, created = #{created}, start_dt = #{dateBegin}, " +
            "end_dt = #{dateEnd} WHERE id = #{id}")
    int update(@NotNull Task task);

    @NotNull
    @Select("SELECT id, name, description, user_id, project_id, status, created, start_dt, end_dt FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<Task> findAll();

    @NotNull
    @Select("SELECT id, name, description, user_id, project_id, status, created, start_dt, end_dt FROM tm_task " +
            "ORDER BY #{sort}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<Task> findAllSort(@Param("sort") @NotNull Sort sort);

    @NotNull
    @Select("SELECT id, name, description, user_id, project_id, status, created, start_dt, end_dt FROM tm_task " +
            "WHERE user_id = #{userId} ORDER BY #{sort}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<Task> findAllSortByUserId(@Param("userId") @Nullable String userId, @Param("sort") @NotNull Sort sort);

    @Delete("DELETE FROM tm_task")
    void clear();

    @Delete("DELETE FROM tm_task WHERE USER_ID = #{userId}")
    void clearByUserId(@Param("userId") @NotNull String userId);

    boolean existsById(@Nullable String id);

    boolean existsByIdUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    @Select("SELECT id, name, description, user_id, project_id, status, created, start_dt, end_dt FROM tm_task " +
            "WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    Task findOneById(@Param("userId") @Nullable String id);

    @Nullable
    @Select("SELECT id, name, description, user_id, project_id, status, created, start_dt, end_dt FROM tm_task " +
            "WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    Task findOneByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Select("SELECT id, name, description, user_id, project_id, status, created, start_dt, end_dt FROM tm_task " +
            "LIMIT 1 OFFSET #{index}")
    Task findOneByIndex(@Param("index") @Nullable Integer index);

    @Nullable
    @Select("SELECT id, name, description, user_id, project_id, status, created, start_dt, end_dt FROM tm_task " +
            "WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    Task findOneByIndexByUserId(@Param("userId") @Nullable String userId, @Param("index") @Nullable Integer index);

    @Nullable
    @Select("SELECT id FROM tm_task WHERE project_Id = #{projectId} AND user_id = #{userId}")
    List<Task> findAllByProjectId(@Param("userId") @Nullable String userId, @Param("projectId") @Nullable String projectId);

    @Delete("DELETE FROM tm_task WHERE id = #{id}")
    int remove(@Nullable Task model);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    int removeByUserId(@Param("userId") @Nullable String userId, @Nullable Task model);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void removeAll(@Nullable Collection<Task> collection);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    void removeAllByProjectId(@Param("userId") @Nullable String userId, @Param("projectId") @NotNull String projectId);

    @Select("SELECT COUNT(1) FROM tm_task")
    long count();

    @Select("SELECT COUNT(1) FROM tm_task WHERE USER_ID = #{userId}")
    long countByUserId(@Param("userId") @Nullable String userId);
}
