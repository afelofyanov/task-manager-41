package ru.tsc.felofyanov.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull SqlSession getSqlSession();

    @NotNull SqlSessionFactory getSqlSessionFactory();

    @NotNull EntityManagerFactory factory();
}
