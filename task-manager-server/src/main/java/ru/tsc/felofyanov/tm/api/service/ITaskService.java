package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnerService<Task> {

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeAllByProjectId(@Nullable String userId, @NotNull String projectId);
}
