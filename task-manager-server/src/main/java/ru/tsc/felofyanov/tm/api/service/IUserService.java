package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.model.User;

public interface IUserService extends IService<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    int removeByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    boolean isEmailExists(@Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User createWithEmail(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User createWithRole(@Nullable String login, @Nullable String password, @Nullable String email, @Nullable Role role);

    @Nullable
    User setPassword(@Nullable String userId, @Nullable String password);

    @Nullable
    User updateUser(
            @Nullable String userId,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);
}
