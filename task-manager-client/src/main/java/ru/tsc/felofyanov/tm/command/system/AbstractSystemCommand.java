package ru.tsc.felofyanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.service.ICommandService;
import ru.tsc.felofyanov.tm.api.service.IPropertyService;
import ru.tsc.felofyanov.tm.command.AbstractCommand;
import ru.tsc.felofyanov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {
    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }
}
