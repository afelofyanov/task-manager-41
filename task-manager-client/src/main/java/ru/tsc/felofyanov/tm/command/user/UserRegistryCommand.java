package ru.tsc.felofyanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.UserRegistryRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-registry";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Registration new user";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE NEW USER]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        System.out.println("ENTER ROLE:");
        System.out.println(Arrays.toString(Role.values()));
        @NotNull final Role role = Role.toRole(TerminalUtil.nextLine());
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken(), login, password, email, role);
        getServiceLocator().getUserEndpoint().registryUser(request);
    }
}
