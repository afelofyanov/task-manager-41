package ru.tsc.felofyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.TaskListRequest;
import ru.tsc.felofyanov.tm.dto.response.TaskListResponse;
import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task list.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));

        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);

        @NotNull final TaskListRequest taskListRequest = new TaskListRequest(getToken(), sort);
        @NotNull final TaskListResponse taskListResponse =
                getServiceLocator().getTaskEndpoint().listTask(taskListRequest);
        @Nullable final List<Task> tasks = taskListResponse.getTasks();

        renderTasks(tasks);
    }
}
