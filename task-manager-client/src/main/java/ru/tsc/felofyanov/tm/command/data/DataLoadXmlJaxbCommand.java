package ru.tsc.felofyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.DataLoadXmlJaxbRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;

public final class DataLoadXmlJaxbCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-load-xml-jaxb";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from xml file(JAXB)";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getDomainEndpoint().loadDataXmlJaxb(new DataLoadXmlJaxbRequest(getToken()));
    }
}
