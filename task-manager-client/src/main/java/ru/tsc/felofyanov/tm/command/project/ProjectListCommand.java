package ru.tsc.felofyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.ProjectListRequest;
import ru.tsc.felofyanov.tm.dto.response.ProjectListResponse;
import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project list.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));

        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);

        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken(), sort);
        @NotNull final ProjectListResponse response =
                getServiceLocator().getProjectEndpoint().listProject(request);
        @NotNull final List<Project> projects = response.getProjects();

        int index = 1;
        System.out.println("[INDEX.NAME]-------------------[ID]--------------------[STATUS]--------[DATE BEGIN]");
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + "." + project);
            index++;
        }
    }
}
